Overview
--------

This library based on [System.Web.Optimization](http://msdn.microsoft.com/en-us/library/system.web.optimization.aspx) and it helps you to solve some of [existing issues](http://aspnetoptimization.codeplex.com/workitem/list/basic) and brings some very useful features such as:

* Custom manifest structure/format

	Currently, original library support only XML-based manifests and also has predefined section of bundles (scripts and styles), but this library helps you to resolve these limitations:

	- You can use your own file format for manifests (the default is JSON format);
	- You can define custom sections for manifests: `templates`, `cultures` and etc.
	- Each manifest can reference some external manifests which helps you to keep manifests more well organized.

* Dependencies management

	If developer changed manifests then they wouldn't be reloaded by original library and to apply these changes you need to restart IIS every time. That makes the process of active development isn't very convenient, but this library helps you to focus on development process and also it automatically handles all of changes for you.

* Dynamic URL support for bundles

	Currently, original library allows you to use static URL schema for bundles `~/scripts/themes/default`, but this library allows you to use dynamic schema `~/scripts/themes/{theme}` for that.

* Nested scanning

	Currently, original library doesn't allow to search files using complex patterns, but this library helps you to do it more easily and you can use the following patterns: `~/folder/**/*.<extension>` or `~/folder/**/<subfolder1>/<subfolder2>/<subfolderN>/*.<extension>`

* Etc.

Documentation
-------------

Is not available yet.

Get started
-----------

All manifests are located in bundles [folder](https://bitbucket.org/mdreizin/aspnet-web-optimization/raw/master/src/Web.Optimization.Site/App_Data/bundles) and also you can find the code snippet which helps you understand how to register them [here](https://bitbucket.org/mdreizin/aspnet-web-optimization/raw/master/src/Web.Optimization.Site/App_Start/BundleConfig.cs).