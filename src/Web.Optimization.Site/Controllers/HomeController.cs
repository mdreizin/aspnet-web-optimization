﻿using System.Web.Mvc;

namespace Web.Optimization.Site.Controllers
{
    public class HomeController : Controller
    {
        #region Methods

        public ActionResult Index()
        {
            return View();
        }

        #endregion
    }
}
