﻿using Web.Optimization.Core;

namespace Web.Optimization.Site
{
    internal static class BundleConfig
    {
        #region Methods

        public static void RegisterBundles()
        {
            BundleManifest.Manifests.Require("~/app_data/bundles/site.manifest");
        }

        #endregion
    }
}