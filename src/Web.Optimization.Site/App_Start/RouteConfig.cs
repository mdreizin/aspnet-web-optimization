﻿using System.Web.Mvc;
using System.Web.Routing;

namespace Web.Optimization.Site
{
    public class RouteConfig
    {
        #region Methods

        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute("home#index", string.Empty, new { controller = "Home", action = "Index" });
        }

        #endregion
    }
}