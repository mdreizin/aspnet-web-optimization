﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web.Optimization;

namespace Web.Optimization.Core
{
    [Serializable]
    public class BundleActivatorDictionary : Dictionary<Type, IBundleActivator>
    {
        #region Fields

        private readonly IDictionary<Type, IList<string>> associations = new Dictionary<Type, IList<string>>();

        private IBundleActivator defaultActivator;

        #endregion

        #region Constructors

        public BundleActivatorDictionary()
        {
        }

        protected BundleActivatorDictionary(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }

        #endregion

        #region Properties

        public IBundleActivator DefaultActivator
        {
            get { return defaultActivator ?? (defaultActivator = new DefaultBundleActivator()); }
            set { defaultActivator = value; }
        }

        #endregion

        #region Methods

        public Type FindAssociatedType(string alias)
        {
            if (string.IsNullOrEmpty(alias))
            {
                throw new ArgumentNullException("alias");
            }

            var association = associations.FirstOrDefault(x => x.Value.Contains(alias, StringComparer.OrdinalIgnoreCase));

            return association.Key;
        }

        public void AddAssociation<TBundle>(string alias)
            where TBundle : Bundle
        {
            if (string.IsNullOrEmpty(alias))
            {
                throw new ArgumentNullException("alias");
            }

            var bundleType = typeof(TBundle);

            IList<string> aliases;

            if (!associations.TryGetValue(bundleType, out aliases))
            {
                aliases = new List<string>();

                associations[bundleType] = aliases;
            }

            if (!aliases.Contains(alias))
            {
                aliases.Add(alias);
            }
        }

        public IBundleActivator GetActivator(Type type)
        {
            if (type == null)
            {
                throw new ArgumentNullException("type");
            }

            IBundleActivator activator;

            TryGetValue(type, out activator);

            return activator ?? DefaultActivator;
        }

        public IBundleActivator GetActivator(string typeName)
        {
            if (string.IsNullOrEmpty(typeName))
            {
                throw new ArgumentNullException("typeName");
            }

            return GetActivator(Type.GetType(typeName, false));
        }

        #endregion
    }
}