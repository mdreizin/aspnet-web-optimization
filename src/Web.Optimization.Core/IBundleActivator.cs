﻿namespace Web.Optimization.Core
{
    public interface IBundleActivator
    {
        #region Methods

        DefaultBundle CreateBundle(BundleDefinition bundleDefinition);

        #endregion
    }
}