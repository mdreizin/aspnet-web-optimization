﻿using System.Collections.Generic;
using System.Web.Optimization;

namespace Web.Optimization.Core
{
    public interface IBundleCache
    {
        #region Methods

        bool IsEnabled(BundleContext context);

        BundleResponse Get(BundleContext context, Bundle bundle);

        void Put(BundleContext context, Bundle bundle, BundleResponse response, IDictionary<string, object> metadata = null);

        #endregion
    }
}