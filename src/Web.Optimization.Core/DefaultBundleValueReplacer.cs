﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.Optimization;

namespace Web.Optimization.Core
{
    internal sealed class DefaultBundleValueReplacer : IBundleValueReplacer
    {
        #region Fields

        private static readonly Regex macrosRegex = new Regex("({([\\w]+)})", RegexOptions.Multiline | RegexOptions.IgnoreCase | RegexOptions.Compiled);

        #endregion

        #region IBundleValueReplacer Members

        public string ReplaceValues(BundleContext bundleContext, string content)
        {
            if (bundleContext == null)
            {
                throw new ArgumentNullException("bundleContext");
            }

            var updatedContent = content;

            if (!string.IsNullOrEmpty(updatedContent))
            {
                var macros = FindMacros(content);
                var provider = new Lazy<IBundleValueProvider>(() => BundleValueProviderFactories.Factories.GetValueProvider(bundleContext));

                foreach (var macro in macros)
                {
                    var value = provider.Value.GetValue(macro.Value);

                    if (value != null)
                    {
                        updatedContent = updatedContent.Replace(macro.Key, value.ToString());
                    }
                }
            }

            return updatedContent;
        }

        #endregion

        #region Methods

        private static IEnumerable<KeyValuePair<string, string>> FindMacros(string content)
        {
            return macrosRegex.Matches(content).OfType<Match>().Select(x => new KeyValuePair<string, string>(x.Groups[1].Value, x.Groups[2].Value));
        }

        #endregion
    }
}