﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace Web.Optimization.Core
{
    public class BundleValueProviderCollection : Collection<IBundleValueProvider>, IBundleValueProvider
    {
        #region Constructors

        public BundleValueProviderCollection()
        {
        }

        public BundleValueProviderCollection(IList<IBundleValueProvider> list) : base(list)
        {
        }

        #endregion

        #region IBundleValueProvider Members

        public object GetValue(string key)
        {
            if (string.IsNullOrEmpty(key))
            {
                throw new ArgumentNullException("key");
            }

            return this.Select(x => new { result = x.GetValue(key) })
                       .Where(x => x.result != null)
                       .Select(x => x.result)
                       .FirstOrDefault();
        }

        #endregion
    }
}