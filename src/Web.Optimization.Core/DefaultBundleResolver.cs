﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;

namespace Web.Optimization.Core
{
    internal sealed class DefaultBundleResolver : IBundleResolver
    {
        #region Fields

        private readonly IBundleResolver originalResolver;

        #endregion

        #region Constructors

        public DefaultBundleResolver(IBundleResolver originalResolver)
        {
            if (originalResolver == null)
            {
                throw new ArgumentNullException("originalResolver");
            }

            this.originalResolver = originalResolver;
        }

        #endregion

        #region IBundleResolver Members

        bool IBundleResolver.IsBundleVirtualPath(string virtualPath)
        {
            return IsBundleVirtualPath(virtualPath) || originalResolver.IsBundleVirtualPath(virtualPath);
        }

        IEnumerable<string> IBundleResolver.GetBundleContents(string virtualPath)
        {
            return GetBundleContents(virtualPath) ?? originalResolver.GetBundleContents(virtualPath);
        }

        string IBundleResolver.GetBundleUrl(string virtualPath)
        {
            return GetBundleUrl(virtualPath) ?? originalResolver.GetBundleUrl(virtualPath);
        }

        #endregion

        #region Methods

        private static Bundle GetBundle(string virtualPath)
        {
            Bundle bundle = null;

            var routeData = BundleManifest.Manifests.RouteCollection.GetRouteData(virtualPath);

            if (routeData != null)
            {
                bundle = BundleManifest.Manifests.BundleCollection.GetBundleFor(routeData);
            }

            return bundle;
        }

        private static bool IsBundleVirtualPath(string virtualPath)
        {
            var bundle = GetBundle(virtualPath);

            return bundle != null;
        }

        private static IEnumerable<string> GetBundleContents(string virtualPath)
        {
            IEnumerable<string> contents = null;

            var bundleResponse = GenerateBundleResponse(virtualPath);

            if (bundleResponse != null)
            {
                contents = bundleResponse.Files.Select(x => x.VirtualFile.VirtualPath);
            }

            return contents;
        }

        private static BundleResponse GenerateBundleResponse(string virtualPath)
        {
            BundleResponse bundleResponse = null;

            var bundle = GetBundle(virtualPath);

            if (bundle != null)
            {
                var bundleContext = BundleContextFactory.CreateBundleContext(virtualPath);

                bundleResponse = bundle.GenerateBundleResponse(bundleContext);
            }

            return bundleResponse;
        }

        private static string GetBundleUrl(string virtualPath)
        {
            var bundleUrl = virtualPath;

            var bundle = GetBundle(virtualPath);

            if (bundle != null)
            {
                if (BundleManifest.Manifests.BundleCollection.UseCdn && !string.IsNullOrEmpty(bundle.CdnPath))
                {
                    bundleUrl = bundle.CdnPath;
                }
                else
                {
                    var bundleResponse = GenerateBundleResponse(virtualPath);
                    var bundleContext = BundleContextFactory.CreateBundleContext(virtualPath);

                    bundleUrl = BundleValueReplacer.Current.ReplaceValues(bundleContext, bundleUrl);

                    if (bundleResponse != null && !string.IsNullOrEmpty(bundleUrl))
                    {
                        var versionHash = HttpServerUtility.UrlTokenEncode(bundleResponse.ComputeHash());

                        bundleUrl = ResolveVirtualPath(virtualPath, bundleContext.HttpContext.Request) + "?v=" + versionHash;
                    }
                }
            }

            return bundleUrl;
        }

        private static string ResolveVirtualPath(string virtualPath, HttpRequestBase httpRequest)
        {
            string relativePath;

            Uri result;

            if (Uri.TryCreate(virtualPath, UriKind.Absolute, out result))
            {
                relativePath = virtualPath;
            }
            else
            {
                relativePath = VirtualPathUtility.ToAbsolute(virtualPath, httpRequest.ApplicationPath);
            }

            return relativePath;
        }

        #endregion
    }
}