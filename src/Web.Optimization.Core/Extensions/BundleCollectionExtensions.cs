﻿using System;
using System.Web.Optimization;
using System.Web.Routing;

namespace Web.Optimization.Core
{
    internal static class BundleCollectionExtensions
    {
        #region Methods

        public static Bundle GetBundleFor(this BundleCollection bundles, RouteData routeData)
        {
            if (bundles == null)
            {
                throw new ArgumentNullException("bundles");
            }

            if (routeData == null)
            {
                throw new ArgumentNullException("routeData");
            }

            Bundle bundle = null;

            var bundleUrl = GetBundleUrl(routeData);

            if (!string.IsNullOrEmpty(bundleUrl))
            {
                bundle = bundles.GetBundleFor(bundleUrl);
            }

            return bundle;
        }

        private static string GetBundleUrl(RouteData routeData)
        {
            if (routeData == null)
            {
                throw new ArgumentNullException("routeData");
            }

            string bundleUrl = null;

            var route = routeData.Route as Route;

            if (route != null)
            {
                bundleUrl = "~/" + route.Url;
            }

            return bundleUrl;
        }

        #endregion
    }
}