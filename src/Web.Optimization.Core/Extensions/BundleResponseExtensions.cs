﻿using System;
using System.Security.Cryptography;
using System.Text;
using System.Web.Optimization;

namespace Web.Optimization.Core
{
    internal static class BundleResponseExtensions
    {
        #region Fields

        private static readonly bool isMonoRuntime = Type.GetType("Mono.Runtime") != null; 

        #endregion

        #region Properties

        private static bool AllowOnlyFipsAlgorithms
        {
            get { return !isMonoRuntime && CryptoConfig.AllowOnlyFipsAlgorithms; }
        }

        #endregion

        #region Methods

        public static byte[] ComputeHash(this BundleResponse bundleResponse)
        {
            if (bundleResponse == null)
            {
                throw new ArgumentNullException("bundleResponse");
            }

            byte[] bytes;

            using (var hashAlgorithm = CreateHashAlgorithm())
            {
                bytes = hashAlgorithm.ComputeHash(Encoding.Unicode.GetBytes(bundleResponse.Content));
            }

            return bytes;
        }

        private static SHA256 CreateHashAlgorithm()
        {
            return AllowOnlyFipsAlgorithms ? new SHA256CryptoServiceProvider() : (SHA256)new SHA256Managed();
        }

        #endregion
    }
}