﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Web.Optimization.Core
{
    internal static class BundleManifestDefinitionExtensions
    {
        #region Methods

        public static void Inspect(this BundleManifestDefinition manifest, Action<BundleDefinition> inspector)
        {
            if (inspector == null)
            {
                throw new ArgumentNullException("inspector");
            }

            if (manifest != null)
            {
                manifest.Inspect(outerManifest =>
                {
                    foreach (var bundles in outerManifest.Bundles)
                    {
                        foreach (var bundle in bundles.Value)
                        {
                            bundle.Manifest = bundle.Manifest ?? outerManifest;

                            if (string.IsNullOrEmpty(bundle.Alias))
                            {
                                bundle.Alias = bundles.Key;
                            }

                            inspector(bundle);
                        }
                    }
                });
            }
        }

        public static IEnumerable<BundleManifestDefinition> GetRelatedManifests(this BundleManifestDefinition manifest)
        {
            if (manifest != null)
            {
                foreach (var outerManifest in manifest.Manifests)
                {
                    if (outerManifest != null)
                    {
                        yield return outerManifest;
                    }

                    foreach (var innerManifest in outerManifest.GetRelatedManifests().Where(x => x != null && !string.IsNullOrEmpty(x.Path)))
                    {
                        yield return innerManifest;
                    }
                }
            }
        }

        private static void Inspect(this BundleManifestDefinition manifest, Action<BundleManifestDefinition> inspector)
        {
            if (inspector == null)
            {
                throw new ArgumentNullException("inspector");
            }

            if (manifest != null)
            {
                inspector(manifest);

                foreach (var outerManifest in manifest.Manifests)
                {
                    outerManifest.Inspect(inspector);
                }
            }
        }

        #endregion
    }
}