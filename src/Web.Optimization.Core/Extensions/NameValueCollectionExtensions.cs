﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;

namespace Web.Optimization.Core
{
    internal static class NameValueCollectionExtensions
    {
        #region Methods

        public static IEnumerable<KeyValuePair<string, string>> ToPairs(this NameValueCollection collection)
        {
            if (collection == null)
            {
                throw new ArgumentNullException("collection");
            }

            return collection.Cast<string>().Select(x => new KeyValuePair<string, string>(x, collection[x]));
        }

        #endregion
    }
}