﻿using System;
using System.Web.Routing;

namespace Web.Optimization.Core
{
    internal static class RouteCollectionExtensions
    {
        #region Methods

        public static Route MapBundle(this RouteCollection routes, string name, string url, object defaults = null, object constraints = null)
        {
            if (routes == null)
            {
                throw new ArgumentNullException("routes");
            }

            if (url == null)
            {
                throw new ArgumentNullException("url");
            }

            var route = new Route(url.Replace("~/", string.Empty), new BundleRouteHandler());

            routes.Add(name, route);

            return route;
        }

        public static RouteData GetRouteData(this RouteCollection routes, string virtualPath)
        {
            if (routes == null)
            {
                throw new ArgumentNullException("routes");
            }

            if (string.IsNullOrEmpty(virtualPath))
            {
                throw new ArgumentNullException("virtualPath");
            }

            return routes.GetRouteData(new EmptyHttpContext(virtualPath));
        }

        #endregion
    }
}