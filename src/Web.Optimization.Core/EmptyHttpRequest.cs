﻿using System.Web;

namespace Web.Optimization.Core
{
    internal sealed class EmptyHttpRequest : HttpRequestBase
    {
        #region Fields

        private readonly string virtualPath;

        #endregion

        #region Constructors

        public EmptyHttpRequest(string virtualPath)
        {
            this.virtualPath = virtualPath;
        }

        #endregion

        #region Overrides of HttpRequestBase

        public override string AppRelativeCurrentExecutionFilePath
        {
            get { return virtualPath; }
        }

        public override string PathInfo
        {
            get { return string.Empty; }
        }

        public override string ApplicationPath
        {
            get { return "/"; }
        }

        #endregion
    }
}