﻿using System;
using System.Web.Mvc;
using System.Web.Optimization;

namespace Web.Optimization.Core.Mvc
{
    public class ReloadBundleManifestsAttribute : ActionFilterAttribute
    {
        #region Overrides of ActionFilterAttribute

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (filterContext == null)
            {
                throw new ArgumentNullException("filterContext");
            }

            BundleManifest.Manifests.SyncAll();
        }

        #endregion
    }
}