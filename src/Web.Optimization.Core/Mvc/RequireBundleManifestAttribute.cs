﻿using System;
using System.Web.Mvc;

namespace Web.Optimization.Core.Mvc
{
    public class RequireBundleManifestAttribute : ActionFilterAttribute
    {
        #region Fields

        private readonly string manifest;

        #endregion

        #region Constructors

        public RequireBundleManifestAttribute(string manifest)
        {
            if (string.IsNullOrEmpty(manifest))
            {
                throw new ArgumentNullException("manifest");
            }

            this.manifest = manifest;
        }

        #endregion

        #region Overrides of ActionFilterAttribute

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (filterContext == null)
            {
                throw new ArgumentNullException("filterContext");
            }

            BundleManifest.Manifests.Require(manifest);
        }

        #endregion
    }
}