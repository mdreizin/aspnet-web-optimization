﻿using System;
using System.Web.Mvc;

namespace Web.Optimization.Core.Mvc
{
    public static class UrlHelperExtensions
    {
        #region Methods

        public static string[] Bundle(this UrlHelper urlHelper, string virtualPath, bool optimized = false)
        {
            if (urlHelper == null)
            {
                throw new ArgumentNullException("urlHelper");
            }

            if (string.IsNullOrEmpty(virtualPath))
            {
                throw new ArgumentNullException("virtualPath");
            }

            return BundleUrlResolver.ResolveBundleUrl(virtualPath, optimized, urlHelper.Content);
        }

        #endregion
    }
}