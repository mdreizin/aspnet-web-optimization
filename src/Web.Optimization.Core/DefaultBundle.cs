﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Caching;
using System.Web.Hosting;
using System.Web.Optimization;

namespace Web.Optimization.Core
{
    public abstract class DefaultBundle : Bundle
    {
        #region Fields

        private readonly IList<BundleItem> items = new List<BundleItem>();

        #endregion

        #region Constructors

        protected DefaultBundle(string virtualPath, string cdnPath, params IBundleTransform[] transforms)
            : base(virtualPath, cdnPath, transforms)
        {
        }

        protected DefaultBundle(string virtualPath, params IBundleTransform[] transforms)
            : this(virtualPath, null, transforms)
        {
        }

        protected DefaultBundle(string virtualPath)
            : this(virtualPath, new IBundleTransform[] { })
        {
        }

        protected DefaultBundle(string virtualPath, string cdnPath)
            : this(virtualPath, cdnPath, new IBundleTransform[] { })
        {
        }

        #endregion

        #region Methods

        public override Bundle Include(params string[] virtualPaths)
        {
            foreach (var virtualPath in virtualPaths)
            {
                Include(virtualPath);
            }

            return this;
        }

        public override Bundle Include(string virtualPath, params IItemTransform[] transforms)
        {
            if (string.IsNullOrEmpty(virtualPath))
            {
                throw new ArgumentNullException("virtualPath");
            }

            var include = BundleDirectoryItem.CreateDirectoryItem(virtualPath) ?? new BundleItem { VirtualPath = virtualPath };

            IncludeWithTransforms(include, transforms);

            return this;
        }

        public override Bundle IncludeDirectory(string directoryVirtualPath, string searchPattern)
        {
            return IncludeDirectoryWithTransforms(directoryVirtualPath, searchPattern);
        }

        public override Bundle IncludeDirectory(string directoryVirtualPath, string searchPattern, bool searchSubdirectories)
        {
            return IncludeDirectoryWithTransforms(directoryVirtualPath, searchPattern, searchSubdirectories);
        }

        public override IEnumerable<BundleFile> EnumerateFiles(BundleContext context)
        {
            if (context == null)
            {
                throw new ArgumentNullException("context");
            }

            var files = new List<BundleFile>();

            foreach (var item in items)
            {
                item.AddFiles(files, context);
            }

            return files;
        }

        public override BundleResponse GenerateBundleResponse(BundleContext context)
        {
            var bundleResponse = CacheLookup(context);

            if (bundleResponse == null)
            {
                bundleResponse = base.GenerateBundleResponse(context);

                UpdateCache(context, bundleResponse);
            }

            return bundleResponse;
        }

        public override BundleResponse CacheLookup(BundleContext context)
        {
            if (context == null)
            {
                throw new ArgumentNullException("context");
            }

            BundleResponse bundleResponse = null;

            if (BundleCache.Current.IsEnabled(context))
            {
                bundleResponse = BundleCache.Current.Get(context, this);
            }

            return bundleResponse;
        }

        public override void UpdateCache(BundleContext context, BundleResponse response)
        {
            if (context == null)
            {
                throw new ArgumentNullException("context");
            }

            if (response == null)
            {
                throw new ArgumentNullException("response");
            }

            if (BundleCache.Current.IsEnabled(context))
            {
                var metadata = new Dictionary<string, object>();
                var paths = new HashSet<string>(StringComparer.OrdinalIgnoreCase);
                var bundleDefinition = BundleManifest.Manifests.GetBundleFor(Path);
                var directories = items.OfType<BundleDirectoryItem>().Select(x => BundleValueReplacer.Current.ReplaceValues(context, x.VirtualPath));
                var manifests = bundleDefinition != null ? BundleManifest.Manifests.GetBundleManifests(bundleDefinition.Path) : Enumerable.Empty<string>();

                foreach (var directory in directories)
                {
                    paths.Add(directory);
                }

                foreach (var manifest in manifests)
                {
                    paths.Add(manifest);
                }

                metadata["virtualPathDependencies"] = GetCacheDependencies(context, paths.ToArray());
                metadata["onRemoveCallback"] = new CacheItemRemovedCallback(OnUpdateCache);

                BundleCache.Current.Put(context, this, response, metadata);
            }
        }

        protected virtual void OnInclude(BundleItem item)
        {
        }

        private static void OnUpdateCache(string key, object value, CacheItemRemovedReason reason)
        {
            BundleManifest.Manifests.Sync = reason == CacheItemRemovedReason.DependencyChanged;
        }

        private static IEnumerable<VirtualFileBase> GetCacheDependencies(BundleContext bundleContext, params string[] virtualPaths)
        {
            var result = new List<VirtualFileBase>();
            var files = new HashSet<string>(StringComparer.OrdinalIgnoreCase);
            var directories = new HashSet<string>(StringComparer.OrdinalIgnoreCase);

            foreach (var virtualPath in virtualPaths)
            {
                var path = BundleValueReplacer.Current.ReplaceValues(bundleContext, virtualPath);

                if (!string.IsNullOrEmpty(path))
                {
                    if (System.IO.Path.HasExtension(path))
                    {
                        var fileName = System.IO.Path.GetFileName(path);

                        if (!string.IsNullOrEmpty(fileName))
                        {
                            files.Add(path);

                            path = path.Replace(fileName, string.Empty);

                            if (!string.IsNullOrEmpty(path))
                            {
                                directories.Add(path);
                            }
                        }
                    }
                    else
                    {
                        directories.Add(path);
                    }
                }
            }

            result.AddRange(directories.Where(BundleTable.VirtualPathProvider.DirectoryExists).Select(BundleTable.VirtualPathProvider.GetDirectory));
            result.AddRange(files.Where(BundleTable.VirtualPathProvider.FileExists).Select(BundleTable.VirtualPathProvider.GetFile));

            return result;
        }

        private Bundle IncludeDirectoryWithTransforms(string directoryVirtualPath, string searchPattern, bool searchSubdirectories = false, params IItemTransform[] transforms)
        {
            if (string.IsNullOrEmpty(directoryVirtualPath))
            {
                throw new ArgumentNullException("directoryVirtualPath");
            }

            if (string.IsNullOrEmpty(searchPattern))
            {
                throw new ArgumentNullException("searchPattern");
            }

            var include = new BundleDirectoryItem
            {
                VirtualPath = directoryVirtualPath,
                SearchPattern = searchPattern,
                SearchSubdirectories = searchSubdirectories
            };

            IncludeWithTransforms(include, transforms);

            return this;
        }

        private void IncludeWithTransforms(BundleItem item, IEnumerable<IItemTransform> transforms)
        {
            if (item != null && !items.Contains(item, BundleItem.VirtualPathComparer))
            {
                foreach (var transform in transforms)
                {
                    item.Transforms.Add(transform);
                }

                OnInclude(item);

                items.Add(item);
            }
        }

        #endregion
    }
}