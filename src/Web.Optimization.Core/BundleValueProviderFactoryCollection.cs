﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web.Optimization;

namespace Web.Optimization.Core
{
    public class BundleValueProviderFactoryCollection : Collection<IBundleValueProviderFactory>
    {
        #region Methods

        public IBundleValueProvider GetValueProvider(BundleContext bundleContext)
        {
            if (bundleContext == null)
            {
                throw new ArgumentNullException("bundleContext");
            }

            var providers = this.Select(x => x.GetValueProvider(bundleContext)).Where(x => x != null).ToList();

            return new BundleValueProviderCollection(providers);
        }

        #endregion
    }
}