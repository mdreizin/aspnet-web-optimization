﻿namespace Web.Optimization.Core
{
    public interface IBundleValueProvider
    {
        #region Methods

        object GetValue(string key);

        #endregion
    }
}