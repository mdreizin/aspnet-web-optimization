﻿using System.Web.Optimization;
using System.Web.Routing;

namespace Web.Optimization.Core
{
    public static class BundleManifest
    {
        #region Fields

        private static readonly IBundleManifestProvider defaultProvider = new DefaultBundleManifestProvider();

        private static readonly BundleManifestDefinitionCollection manifests = new BundleManifestDefinitionCollection(BundleTable.Bundles, RouteTable.Routes);

        private static IBundleManifestProvider currentProvider;

        #endregion

        #region Properties

        public static IBundleManifestProvider Provider
        {
            get { return currentProvider ?? defaultProvider; }
            set { currentProvider = value; }
        }

        public static BundleManifestDefinitionCollection Manifests
        {
            get { return manifests; }
        }

        #endregion
    }
}