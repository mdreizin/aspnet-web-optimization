﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web.Optimization;

namespace Web.Optimization.Core
{
    internal sealed class DefaultBundleBuilder : IBundleBuilder
    {
        #region Fieilds

        public static readonly IBundleBuilder Instance = new DefaultBundleBuilder();

        #endregion

        #region IBundleBuilder Members

        public string BuildBundleContent(Bundle bundle, BundleContext context, IEnumerable<BundleFile> files)
        {
            if (bundle == null)
            {
                throw new ArgumentNullException("bundle");
            }

            if (context == null)
            {
                throw new ArgumentNullException("context");
            }

            var builder = new StringBuilder();

            if (files != null)
            {
                var concatenationToken = !string.IsNullOrEmpty(bundle.ConcatenationToken) ? bundle.ConcatenationToken : Environment.NewLine;

                foreach (var file in files)
                {
                    builder.Append(file.ApplyTransforms());
                    builder.Append(concatenationToken);
                }
            }

            return builder.ToString();
        }

        #endregion
    }
}