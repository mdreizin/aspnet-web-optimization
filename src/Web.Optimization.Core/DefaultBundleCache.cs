﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Web;
using System.Web.Caching;
using System.Web.Hosting;
using System.Web.Optimization;

namespace Web.Optimization.Core
{
    internal sealed class DefaultBundleCache : IBundleCache
    {
        #region IBundleCache Members

        public bool IsEnabled(BundleContext context)
        {
            if (context == null)
            {
                throw new ArgumentNullException("context");
            }

            return HttpRuntime.Cache != null && !context.EnableInstrumentation;
        }

        public BundleResponse Get(BundleContext context, Bundle bundle)
        {
            if (context == null)
            {
                throw new ArgumentNullException("context");
            }

            return HttpRuntime.Cache[bundle.GetCacheKey(context)] as BundleResponse;
        }

        [SuppressMessage("StyleCop.CSharp.NamingRules", "SA1305:FieldNamesMustNotUseHungarianNotation", Justification = "The variable name 'onRemoveCallback' has naming convention according to the signature of the 'Cache.Insert' method (please see http://msdn.microsoft.com/en-us/library/05kd8d77(v=vs.100).aspx)")]
        public void Put(BundleContext context, Bundle bundle, BundleResponse response, IDictionary<string, object> metadata = null)
        {
            if (context == null)
            {
                throw new ArgumentNullException("context");
            }

            if (bundle == null)
            {
                throw new ArgumentNullException("bundle");
            }

            if (response == null)
            {
                throw new ArgumentNullException("response");
            }

            metadata = metadata ?? new Dictionary<string, object>();

            var cacheKey = bundle.GetCacheKey(context);
            var cacheDependency = GetValue(metadata, "cacheDependency", () => BuildDefaultCacheDependency(context, response, GetValue<IEnumerable<VirtualFileBase>>(metadata, "virtualPathDependencies")));
            var absoluteExpiration = GetValue(metadata, "absoluteExpiration", () => Cache.NoAbsoluteExpiration);
            var slidingExpiration = GetValue(metadata, "slidingExpiration", () => Cache.NoSlidingExpiration);
            var priority = GetValue(metadata, "priority", () => CacheItemPriority.Default);
            var onRemoveCallback = GetValue<CacheItemRemovedCallback>(metadata, "onRemoveCallback");

            HttpRuntime.Cache.Insert(cacheKey, response, cacheDependency, absoluteExpiration, slidingExpiration, priority, onRemoveCallback);
        }

        #endregion

        #region Methods

        private static CacheDependency BuildDefaultCacheDependency(BundleContext context, BundleResponse response, IEnumerable<VirtualFileBase> dependencies)
        {
            var list = new List<string>();

            list.AddRange(response.Files.Select(x => x.VirtualFile.VirtualPath));

            if (dependencies != null)
            {
                list.AddRange(dependencies.Select(x => x.VirtualPath));
            }

            return BundleTable.VirtualPathProvider.GetCacheDependency(context.BundleVirtualPath, list, DateTime.UtcNow);
        }

        private static T GetValue<T>(IDictionary<string, object> dictionary, string key, Func<T> fallbackValue = null)
        {
            var result = default(T);

            object value;

            if (dictionary.TryGetValue(key, out value))
            {
                if (value is T)
                {
                    result = (T)value;
                }
            }
            else
            {
                if (fallbackValue != null)
                {
                    result = fallbackValue();
                }
            }

            return result;
        }

        #endregion
    }
}