﻿using System.Web.Optimization;

namespace Web.Optimization.Core
{
    public interface IBundleValueReplacer
    {
        #region Methods

        string ReplaceValues(BundleContext bundleContext, string content);

        #endregion
    }
}