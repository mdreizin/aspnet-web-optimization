﻿using System;
using System.Collections.Generic;

namespace Web.Optimization.Core
{
    public class BundleDictionaryValueProvider<TValue> : IBundleValueProvider
    {
        #region Fields

        private readonly IDictionary<string, TValue> values = new Dictionary<string, TValue>(StringComparer.OrdinalIgnoreCase);

        #endregion

        #region Constructors

        public BundleDictionaryValueProvider(IEnumerable<KeyValuePair<string, TValue>> dictionary)
        {
            if (dictionary == null)
            {
                throw new ArgumentNullException("dictionary");
            }

            foreach (var value in dictionary)
            {
                values.Add(value);
            }
        }

        #endregion

        #region IBundleValueProvider Members

        public object GetValue(string key)
        {
            TValue value;

            values.TryGetValue(key, out value);

            return value;
        }

        #endregion
    }
}