﻿using System;
using System.Collections.Specialized;
using System.Web.Optimization;

namespace Web.Optimization.Core
{
    public abstract class BundleNameValueCollectionProviderFactory : IBundleValueProviderFactory
    {
        #region IBundleValueProviderFactory Members

        public IBundleValueProvider GetValueProvider(BundleContext bundleContext)
        {
            if (bundleContext == null)
            {
                throw new ArgumentNullException("bundleContext");
            }

            var collection = GetNameValueCollection(bundleContext);

            return collection != null ? new BundleNameValueCollectionProvider(collection) : null;
        }

        #endregion

        #region Methods

        protected abstract NameValueCollection GetNameValueCollection(BundleContext bundleContext);

        #endregion
    }
}