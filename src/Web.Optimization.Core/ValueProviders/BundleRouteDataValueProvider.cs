﻿using System.Web.Routing;

namespace Web.Optimization.Core
{
    public class BundleRouteDataValueProvider : BundleDictionaryValueProvider<object>
    {
        #region Constructors

        public BundleRouteDataValueProvider(RouteData routeData)
            : base(routeData.Values)
        {
        }

        #endregion
    }
}