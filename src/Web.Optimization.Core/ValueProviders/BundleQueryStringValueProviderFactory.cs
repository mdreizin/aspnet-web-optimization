﻿using System.Collections.Specialized;
using System.Web.Optimization;

namespace Web.Optimization.Core
{
    public class BundleQueryStringValueProviderFactory : BundleNameValueCollectionProviderFactory
    {
        #region Overrides of BundleNameValueCollectionProviderFactory

        protected override NameValueCollection GetNameValueCollection(BundleContext bundleContext)
        {
            return bundleContext.HttpContext.Request.QueryString;
        }

        #endregion
    }
}