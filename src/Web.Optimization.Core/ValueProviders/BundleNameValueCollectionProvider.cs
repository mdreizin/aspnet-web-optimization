﻿using System.Collections.Specialized;

namespace Web.Optimization.Core
{
    public class BundleNameValueCollectionProvider : BundleDictionaryValueProvider<string>
    {
        #region Constructors

        public BundleNameValueCollectionProvider(NameValueCollection collection)
            : base(collection.ToPairs())
        {
        }

        #endregion
    }
}