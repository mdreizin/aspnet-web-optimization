﻿using System;
using System.Web;
using System.Web.Optimization;
using System.Web.Routing;

namespace Web.Optimization.Core
{
    public class BundleRouteDataValueProviderFactory : IBundleValueProviderFactory
    {
        #region IBundleValueProviderFactory Members

        public IBundleValueProvider GetValueProvider(BundleContext bundleContext)
        {
            if (bundleContext == null)
            {
                throw new ArgumentNullException("bundleContext");
            }

            var providers = new BundleValueProviderCollection();

            var routeData = GetRouteData(bundleContext.HttpContext);
            var bundleRouteData = GetRouteData(bundleContext.BundleVirtualPath);

            if (routeData != null)
            {
                providers.Add(new BundleRouteDataValueProvider(routeData));
            }

            if (bundleRouteData != null)
            {
                providers.Add(new BundleRouteDataValueProvider(bundleRouteData));
            }

            return providers;
        }

        #endregion

        #region Methods

        private static RouteData GetRouteData(HttpContextBase httpContext)
        {
            return BundleManifest.Manifests.RouteCollection.GetRouteData(httpContext);
        }

        private static RouteData GetRouteData(string virtualPath)
        {
            return BundleManifest.Manifests.RouteCollection.GetRouteData(virtualPath);
        }

        #endregion
    }
}