﻿namespace Web.Optimization.Core
{
    public static class BundleValueProviderFactories
    {
        #region Fields

        private static BundleValueProviderFactoryCollection factories;

        #endregion

        #region Properties

        public static BundleValueProviderFactoryCollection Factories
        {
            get { return factories ?? (factories = CreateDefaultFactories()); }
        }

        #endregion

        #region Methods

        private static BundleValueProviderFactoryCollection CreateDefaultFactories()
        {
            var defaultFactories = new BundleValueProviderFactoryCollection();

            defaultFactories.Add(new BundleRouteDataValueProviderFactory());
            defaultFactories.Add(new BundleQueryStringValueProviderFactory());

            return defaultFactories;
        }

        #endregion
    }
}