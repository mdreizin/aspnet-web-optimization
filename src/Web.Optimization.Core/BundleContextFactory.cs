﻿using System;
using System.Web;
using System.Web.Optimization;

namespace Web.Optimization.Core
{
    internal static class BundleContextFactory
    {
        #region Methods

        public static BundleContext CreateBundleContext(string virtualPath)
        {
            var httpContext = HttpContext.Current != null ? new HttpContextWrapper(HttpContext.Current) : null;

            return CreateBundleContext(virtualPath, httpContext);
        }

        public static BundleContext CreateBundleContext(string virtualPath, HttpContextBase httpContext)
        {
            if (string.IsNullOrEmpty(virtualPath))
            {
                throw new ArgumentNullException("virtualPath");
            }

            httpContext = httpContext ?? new EmptyHttpContext(virtualPath);

            var bundleContext = new BundleContext(httpContext, BundleTable.Bundles, virtualPath)
            {
                EnableInstrumentation = !BundleTable.EnableOptimizations
            };

            return bundleContext;
        }

        #endregion
    }
}