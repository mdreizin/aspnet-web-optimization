﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Hosting;
using System.Web.Optimization;

namespace Web.Optimization.Core
{
    public class BundleDirectoryItem : BundleItem
    {
        #region Fields

        private const string SubdirectoriesWildcard = "**";

        private const string DirectoriesWildcard = "*.";

        private const RegexOptions DefaultRegexOptions = RegexOptions.IgnoreCase | RegexOptions.Compiled;

        #endregion

        #region Properties

        public string SearchPattern { get; set; }

        public bool SearchSubdirectories { get; set; }

        #endregion

        #region Overrides of BundleItem

        public override void AddFiles(IList<BundleFile> files, BundleContext bundleContext)
        {
            if (files == null)
            {
                throw new ArgumentNullException("files");
            }

            if (bundleContext == null)
            {
                throw new ArgumentNullException("bundleContext");
            }

            var virtualPath = BundleValueReplacer.Current.ReplaceValues(bundleContext, VirtualPath);

            if (BundleTable.VirtualPathProvider.DirectoryExists(virtualPath))
            {
                var searchPattern = BundleValueReplacer.Current.ReplaceValues(bundleContext, SearchPattern);
                var virtualDirectory = BundleTable.VirtualPathProvider.GetDirectory(virtualPath);
                var directoryContext = DirectoryContext.CreateContext(this, virtualDirectory, bundleContext);

                directoryContext.FileFilter = BuildFileFilter(searchPattern);

                ProcessDirectory(directoryContext, files);

                if (SearchSubdirectories)
                {
                    ProcessAllSubdirectories(directoryContext, files);
                }
            }
        }

        #endregion

        #region Methods

        internal static BundleDirectoryItem CreateDirectoryItem(string virtualPath)
        {
            BundleDirectoryItem directoryItem = null;

            if (!string.IsNullOrEmpty(virtualPath))
            {
                var fileName = Path.GetFileName(virtualPath);

                if (!string.IsNullOrEmpty(fileName) && fileName.Contains(DirectoriesWildcard))
                {
                    var searchSubdirectories = virtualPath.Contains(SubdirectoriesWildcard);
                    var searchParts = virtualPath.Split(new[] { SubdirectoriesWildcard }, StringSplitOptions.RemoveEmptyEntries);

                    if (searchParts.Length == 2)
                    {
                        var searchPath = VirtualPathUtility.RemoveTrailingSlash(searchParts[0]);
                        var searchPattern = searchParts[1].Trim('/');

                        directoryItem = new BundleDirectoryItem
                        {
                            VirtualPath = searchPath,
                            SearchPattern = searchPattern,
                            SearchSubdirectories = searchSubdirectories
                        };
                    }
                }
            }

            return directoryItem;
        }

        private static Func<VirtualFile, bool> BuildFileFilter(string searchPattern)
        {
            var isWildcard = searchPattern.StartsWith(DirectoriesWildcard, StringComparison.OrdinalIgnoreCase);
            var regex = isWildcard ? BuildWildcardRegex(searchPattern) : BuildSuffixRegex(searchPattern);

            return x => !string.IsNullOrEmpty(x.Name) && !string.IsNullOrEmpty(x.VirtualPath) && regex.IsMatch(isWildcard ? x.Name : x.VirtualPath);
        }

        private static string BuildSearchPattern(string searchPattern)
        {
            searchPattern = searchPattern.Replace("*", "<star>");
            searchPattern = Regex.Escape(searchPattern);
            searchPattern = searchPattern.Replace("<star>", ".*");

            return searchPattern;
        }

        private static Regex BuildWildcardRegex(string searchPattern)
        {
            return new Regex("^" + BuildSearchPattern(searchPattern) + "$", DefaultRegexOptions);
        }

        private static Regex BuildSuffixRegex(string searchPattern)
        {
            return new Regex(BuildSearchPattern(searchPattern) + "$", DefaultRegexOptions);
        }

        private static void ProcessAllSubdirectories(DirectoryContext directoryContext, ICollection<BundleFile> files)
        {
            var directories = directoryContext.VirtualDirectory.Directories.OfType<VirtualDirectory>();

            foreach (var subdirectoryContext in directories.Select(x => DirectoryContext.CreateContext(directoryContext, x)))
            {
                ProcessDirectory(subdirectoryContext, files);

                ProcessAllSubdirectories(subdirectoryContext, files);
            }
        }

        private static void ProcessDirectory(DirectoryContext directoryContext, ICollection<BundleFile> files)
        {
            var directoryFiles = GetNotIgnoredFiles(directoryContext).Where(x => BundleTable.VirtualPathProvider.FileExists(x.IncludedVirtualPath));

            foreach (var file in directoryFiles)
            {
                files.Add(file);
            }
        }

        private static IEnumerable<BundleFile> GetNotIgnoredFiles(DirectoryContext directoryContext)
        {
            var bundleContext = directoryContext.BundleContext;
            var directoryFilter = bundleContext.BundleCollection.DirectoryFilter;
            var directoryFiles = GetAllFiles(directoryContext);

            return directoryFilter.FilterIgnoredFiles(bundleContext, directoryFiles);
        }

        private static IEnumerable<BundleFile> GetAllFiles(DirectoryContext directoryContext)
        {
            var virtualDirectory = directoryContext.VirtualDirectory;
            var fileFilter = directoryContext.FileFilter;
            var directoryFiles = virtualDirectory.Files.OfType<VirtualFile>().Where(fileFilter);

            return directoryFiles.Select(x => GetFile(directoryContext, x)).Where(x => x != null);
        }

        private static BundleFile GetFile(DirectoryContext directoryContext, VirtualFile virtualFile)
        {
            BundleFile bundleFile = null;

            if (!string.IsNullOrEmpty(virtualFile.Name))
            {
                var virtualPath = directoryContext.CombinePaths(virtualFile.Name);

                virtualPath = BundleValueReplacer.Current.ReplaceValues(directoryContext.BundleContext, virtualPath);

                bundleFile = new BundleFile(virtualPath, virtualFile, directoryContext.Transforms);
            }

            return bundleFile;
        }

        #endregion

        #region Nested type: DirectoryContext

        private class DirectoryContext
        {
            #region Properties

            public BundleContext BundleContext { get; private set; }

            public VirtualDirectory VirtualDirectory { get; private set; }

            public Func<VirtualFile, bool> FileFilter { get; set; }

            public IList<IItemTransform> Transforms { get; private set; }

            private string VirtuaPath { get; set; }

            #endregion

            #region Methods

            public static DirectoryContext CreateContext(BundleItem item, VirtualDirectory virtualDirectory, BundleContext bundleContext)
            {
                return new DirectoryContext
                {
                    VirtuaPath = item.VirtualPath,
                    VirtualDirectory = virtualDirectory,
                    BundleContext = bundleContext,
                    Transforms = item.Transforms
                };
            }

            public static DirectoryContext CreateContext(DirectoryContext directoryContext, VirtualDirectory subdirectory)
            {
                DirectoryContext subdirectoryContext = null;

                if (!string.IsNullOrEmpty(subdirectory.Name))
                {
                    subdirectoryContext = new DirectoryContext
                    {
                        VirtualDirectory = subdirectory,
                        VirtuaPath = directoryContext.VirtuaPath,
                        FileFilter = directoryContext.FileFilter,
                        BundleContext = directoryContext.BundleContext,
                        Transforms = directoryContext.Transforms
                    };

                    var virtuaPath = subdirectoryContext.CombinePaths(subdirectory.Name);

                    subdirectoryContext.VirtuaPath = virtuaPath;
                }

                return subdirectoryContext;
            }

            public string CombinePaths(params string[] relativePaths)
            {
                var virtualPath = VirtuaPath;

                if (relativePaths != null)
                {
                    virtualPath = VirtualPathUtility.AppendTrailingSlash(VirtuaPath) + string.Join("/", relativePaths);
                }

                return virtualPath;
            }

            #endregion
        }

        #endregion
    }
}