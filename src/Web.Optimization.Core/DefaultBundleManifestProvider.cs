﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Hosting;
using Newtonsoft.Json;

namespace Web.Optimization.Core
{
    internal sealed class DefaultBundleManifestProvider : IBundleManifestProvider
    {
        #region IBundleManifestProvider Members

        public BundleManifestDefinition LoadManifest(string path)
        {
            return LoadManifest<BundleManifestDefinition>(path, new List<string>());
        }

        #endregion

        #region Methods

        private static TManifest LoadManifest<TManifest>(string path, ICollection<string> visited)
            where TManifest : BundleManifestDefinition
        {
            TManifest manifest = null;

            if (!string.IsNullOrEmpty(path) && !visited.Contains(path))
            {
                manifest = ParseManifest<TManifest>(ReadManifest(path));

                if (manifest != null)
                {
                    visited.Add(path);

                    manifest.Path = path;

                    foreach (var bundles in manifest.Bundles)
                    {
                        foreach (var bundle in bundles.Value)
                        {
                            bundle.Manifest = manifest;

                            if (string.IsNullOrEmpty(bundle.Alias))
                            {
                                bundle.Alias = bundles.Key;
                            }
                        }
                    }

                    var manifests = manifest.Manifests.Where(x => x != null).Select(x => LoadManifest<TManifest>(x.Path, visited)).Where(x => x != null).ToList();

                    manifest.Manifests = new List<BundleManifestDefinition>(manifests);
                }
            }

            return manifest;
        }

        private static string ReadManifest(string path)
        {
            string content = null;

            var physicalPath = !string.IsNullOrEmpty(path) ? HostingEnvironment.MapPath(path) : path;

            if (!string.IsNullOrEmpty(physicalPath) && File.Exists(physicalPath))
            {
                try
                {
                    content = File.ReadAllText(physicalPath);
                }
                catch (Exception)
                {
                    content = null;
                }
            }

            return content;
        }

        private static TManifest ParseManifest<TManifest>(string content)
            where TManifest : BundleManifestDefinition
        {
            TManifest manifest = null;

            if (!string.IsNullOrEmpty(content))
            {
                try
                {
                    manifest = JsonConvert.DeserializeObject<TManifest>(content);
                }
                catch (Exception)
                {
                    manifest = null;
                }
            }

            return manifest;
        }

        #endregion
    }
}