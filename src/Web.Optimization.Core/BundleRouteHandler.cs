﻿using System.Web;
using System.Web.Routing;

namespace Web.Optimization.Core
{
    internal sealed class BundleRouteHandler : IRouteHandler
    {
        #region IRouteHandler Members

        public IHttpHandler GetHttpHandler(RequestContext requestContext)
        {
            return new BundleHttpHandler(requestContext);
        }

        #endregion
    }
}