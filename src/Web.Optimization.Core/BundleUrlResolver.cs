﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Optimization;

namespace Web.Optimization.Core
{
    public static class BundleUrlResolver
    {
        #region Fields

        private static readonly Func<string, string> defaultResolver = x => x;

        #endregion

        #region Methods

        public static string[] ResolveBundleUrl(string virtualPath, bool optimized = false, Func<string, string> resolver = null)
        {
            if (string.IsNullOrEmpty(virtualPath))
            {
                throw new ArgumentNullException("virtualPath");
            }

            var urls = new HashSet<string>(StringComparer.OrdinalIgnoreCase);

            resolver = resolver ?? defaultResolver;

            if (BundleResolver.Current.IsBundleVirtualPath(virtualPath))
            {
                var debug = !BundleTable.EnableOptimizations && !optimized;

                if (debug)
                {
                    var contents = BundleResolver.Current.GetBundleContents(virtualPath);

                    if (contents != null)
                    {
                        foreach (var content in contents.Where(content => !string.IsNullOrEmpty(content)))
                        {
                            urls.Add(resolver(content));
                        }
                    }
                }
                else
                {
                    var url = BundleResolver.Current.GetBundleUrl(virtualPath);

                    if (!string.IsNullOrEmpty(url))
                    {
                        urls.Add(resolver(url));
                    }
                }
            }
            else
            {
                urls.Add(resolver(virtualPath));
            }

            return urls.ToArray();
        }

        #endregion
    }
}