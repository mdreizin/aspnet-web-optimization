﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Optimization;

namespace Web.Optimization.Core.Transforms
{
    public class JsClosureWrapper : IBundleFilter
    {
        #region Fields

        private const string ConcatenationToken = ",";

        private static readonly IDictionary<string, KeyValuePair<string, string>> emptyClosureVariables = new Dictionary<string, KeyValuePair<string, string>>();

        #endregion

        #region Properties

        protected virtual IDictionary<string, KeyValuePair<string, string>> ClosureVariables
        {
            get { return emptyClosureVariables; }
        }

        #endregion

        #region IBundleFilter Members

        public void Process(BundleContext context, BundleResponse response)
        {
            if (response == null)
            {
                throw new ArgumentNullException("response");
            }

            response.Content = ProcessCore(response.Content);
        }

        public string Process(string includedVirtualPath, string input)
        {
            return ProcessCore(input);
        }

        #endregion

        #region Methods

        protected virtual void OpenClosure(StringBuilder builder)
        {
            var scope = GenerateClosureScope(x => x.Value);

            builder.AppendFormat("(function({0}) {{", scope).AppendLine()
                   .AppendLine("'use strict';");
        }

        protected virtual void RenderBody(StringBuilder builder, string content)
        {
        }

        protected virtual void CloseClosure(StringBuilder builder)
        {
            var scope = GenerateClosureScope(x => x.Key);

            builder.AppendFormat("}})({0});", scope);
        }

        private string ProcessCore(string content)
        {
            return WrapWithClosure(content);
        }

        private string WrapWithClosure(string content)
        {
            var builder = new StringBuilder();

            if (!string.IsNullOrEmpty(content))
            {
                OpenClosure(builder);
                RenderBody(builder, content);
                CloseClosure(builder);
            }

            return builder.ToString();
        }

        private string GenerateClosureScope(Func<KeyValuePair<string, string>, string> selector)
        {
            var varibales = ClosureVariables ?? emptyClosureVariables;
            var values = varibales.Select(x => selector(x.Value));

            return string.Join(ConcatenationToken, values);
        }

        #endregion
    }
}