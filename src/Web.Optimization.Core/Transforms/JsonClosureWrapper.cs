﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Web.Optimization.Core.Transforms
{
    public class JsonClosureWrapper : JsClosureWrapper
    {
        #region Fields

        private readonly string moduleName;

        private readonly KeyValuePair<string, string> extender;

        private IDictionary<string, KeyValuePair<string, string>> closureVariables;

        #endregion

        #region Constructors

        public JsonClosureWrapper(string moduleName) : this(moduleName, new KeyValuePair<string, string>("_", "extend"))
        {
        }

        public JsonClosureWrapper(string moduleName, KeyValuePair<string, string> extender)
        {
            if (string.IsNullOrEmpty(moduleName))
            {
                throw new ArgumentNullException("moduleName");
            }

            this.moduleName = moduleName;

            this.extender = extender;
        }

        #endregion

        #region Properties

        protected override IDictionary<string, KeyValuePair<string, string>> ClosureVariables
        {
            get
            {
                if (closureVariables == null)
                {
                    closureVariables = new Dictionary<string, KeyValuePair<string, string>>();

                    closureVariables.Add("root", new KeyValuePair<string, string>("this", "root"));

                    if (!string.IsNullOrEmpty(extender.Key))
                    {
                        closureVariables.Add("extender", new KeyValuePair<string, string>(extender.Key, extender.Key));
                    }
                }

                return closureVariables;
            }
        }

        #endregion

        #region Methods

        protected override void RenderBody(StringBuilder builder, string content)
        {
            var root = ClosureVariables["root"];
            var ns = !string.IsNullOrEmpty(extender.Key) ? extender.Key : root.Key;

            builder.AppendFormat("var {0} = {1};", moduleName, content).AppendLine()
                   .AppendFormat("if ({0}.{1}) {{", root.Value, moduleName).AppendLine()
                   .AppendFormat("{0}.{1}({2}.{3}, {3});", ns, extender.Value, root.Value, moduleName).AppendLine()
                   .AppendLine("} else {")
                   .AppendFormat("{0}.{1} = {1};", root.Value, moduleName).AppendLine()
                   .AppendLine("}");
        }

        #endregion
    }
}