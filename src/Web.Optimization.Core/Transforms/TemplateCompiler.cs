﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Optimization;

namespace Web.Optimization.Core.Transforms
{
    public class TemplateCompiler : IBundleFilter
    {
        #region Fields

        private static readonly Regex syntaxRegex = new Regex("{{tmpl[\\s]+\"(.*?)\"}}([\\s\\S]*?){{/tmpl}}", RegexOptions.Multiline | RegexOptions.IgnoreCase | RegexOptions.Compiled);

        private readonly string compiler;

        #endregion

        #region Constructors

        public TemplateCompiler()
        {
        }

        public TemplateCompiler(string compiler)
        {
            if (string.IsNullOrEmpty(compiler))
            {
                throw new ArgumentNullException("compiler");
            }

            this.compiler = compiler;
        }

        #endregion

        #region IBundleFilter Members

        public void Process(BundleContext context, BundleResponse response)
        {
            if (response == null)
            {
                throw new ArgumentNullException("response");
            }

            response.Content = ProcessCore(response.Content);
        }

        public string Process(string includedVirtualPath, string input)
        {
            return ProcessCore(input);
        }

        #endregion

        #region Methods

        protected virtual KeyValuePair<string, string>[] ParseTemplates(string content)
        {
            var templates = new Dictionary<string, string>();

            if (!string.IsNullOrEmpty(content))
            {
                var matches = syntaxRegex.Matches(content);

                templates = matches.OfType<Match>().ToDictionary(x => x.Groups[1].Value.Trim(), x => x.Groups[2].Value.Trim());
            }

            return templates.ToArray();
        }

        protected virtual string CompileTemplates(params KeyValuePair<string, string>[] templates)
        {
            var builder = new StringBuilder();
            var length = templates.Length;
            var compile = !string.IsNullOrEmpty(compiler);

            builder.Append("{");

            for (var i = 0; i < length; i++)
            {
                var template = templates[i];

                builder.AppendFormat("\"{0}\":", template.Key)
                       .AppendFormat(compile ? compiler : "'{0}'", HttpUtility.JavaScriptStringEncode(template.Value));

                if (i < length - 1)
                {
                    builder.AppendLine(",");
                }
            }

            builder.Append("}");

            return builder.ToString();
        }

        private string ProcessCore(string content)
        {
            return CompileTemplates(ParseTemplates(content));
        }

        #endregion
    }
}