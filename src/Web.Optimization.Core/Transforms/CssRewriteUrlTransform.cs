﻿using System;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Optimization;

namespace Web.Optimization.Core.Transforms
{
    public class CssRewriteUrlTransform : IItemTransform
    {
        #region Fields

        private const string DataPrefix = "data:";

        private static readonly Regex urlRegex = new Regex("url\\(['\"]?(?<url>[^)]+?)['\"]?\\)", RegexOptions.Multiline | RegexOptions.IgnoreCase | RegexOptions.Compiled);

        #endregion

        #region IItemTransform Members

        public string Process(string includedVirtualPath, string input)
        {
            if (string.IsNullOrEmpty(includedVirtualPath))
            {
                throw new ArgumentNullException("includedVirtualPath");
            }

            return ProcessCore(includedVirtualPath, input);
        }

        #endregion

        #region Methods

        private static string ProcessCore(string virtualPath, string content)
        {
            string appPath = null;

            if (HttpContext.Current != null)
            {
                appPath = HttpContext.Current.Request.ApplicationPath;
            }

            var baseUrl = VirtualPathUtility.GetDirectory(virtualPath.Substring(1));

            if (!string.IsNullOrEmpty(appPath) && !appPath.Equals("/", StringComparison.OrdinalIgnoreCase))
            {
                baseUrl = appPath + baseUrl;
            }

            return ConvertUrlsToAbsolute(baseUrl, content);
        }

        private static string ConvertUrlsToAbsolute(string baseUrl, string content)
        {
            return string.IsNullOrWhiteSpace(content) ? content : urlRegex.Replace(content, x => "url(" + RebaseUrlToAbsolute(baseUrl, x.Groups["url"].Value) + ")");
        }

        private static string RebaseUrlToAbsolute(string baseUrl, string url)
        {
            string absoluteUrl;

            if (string.IsNullOrWhiteSpace(url) || string.IsNullOrWhiteSpace(baseUrl) || url.StartsWith("/", StringComparison.OrdinalIgnoreCase))
            {
                absoluteUrl = url;
            }
            else
            {
                if (url.StartsWith(DataPrefix, StringComparison.OrdinalIgnoreCase))
                {
                    absoluteUrl = url;
                }
                else
                {
                    baseUrl = VirtualPathUtility.AppendTrailingSlash(baseUrl);

                    absoluteUrl = VirtualPathUtility.ToAbsolute(baseUrl + url);
                }

                if (!string.IsNullOrEmpty(absoluteUrl))
                {
                    absoluteUrl = absoluteUrl.Trim('\"', '\'');
                }
            }

            return "\"" + absoluteUrl + "\"";
        }

        #endregion
    }
}