﻿using System;
using System.Text.RegularExpressions;
using System.Web.Optimization;

namespace Web.Optimization.Core.Transforms
{
    public class HtmlMinify : IBundleFilter
    {
        #region Fields

        private static readonly Regex betweenTagsRegex = new Regex(@">\s+<", RegexOptions.Compiled);

        private static readonly Regex lineBreaksRegex = new Regex(@"\n\s+", RegexOptions.Compiled);

        #endregion

        #region IBundleTransform Members

        public void Process(BundleContext context, BundleResponse response)
        {
            if (response == null)
            {
                throw new ArgumentNullException("response");
            }

            response.Content = ProcessCore(response.Content);
        }

        #endregion

        #region IItemTransform Members

        public string Process(string includedVirtualPath, string input)
        {
            return ProcessCore(input);
        }

        #endregion

        #region Methods

        protected virtual string RemoveWhitespace(string content)
        {
            if (!string.IsNullOrEmpty(content))
            {
                content = betweenTagsRegex.Replace(content, "><");
                content = lineBreaksRegex.Replace(content, string.Empty);
                content = content.Replace("\n", string.Empty);
                content = content.Replace("\r", string.Empty);
            }

            return content;
        }

        private string ProcessCore(string content)
        {
            return RemoveWhitespace(content);
        }

        #endregion
    }
}