﻿using System;
using System.Text;
using System.Web.Optimization;

namespace Web.Optimization.Core.Transforms
{
    public class JsonCombine : IBundleFilter
    {
        #region IBundleFilter Members

        public void Process(BundleContext context, BundleResponse response)
        {
            if (response == null)
            {
                throw new ArgumentNullException("response");
            }

            response.Content = BuildContent(response.Content);
        }

        public string Process(string includedVirtualPath, string input)
        {
            return BuildContentItem(input);
        }

        #endregion

        #region Methods

        protected virtual string BuildContent(string content)
        {
            var builder = new StringBuilder();

            builder.Append("{");

            if (!string.IsNullOrEmpty(content))
            {
                builder.Append(content.Trim(','));
            }

            builder.Append("}");

            return builder.ToString();
        }

        protected virtual string BuildContentItem(string content)
        {
            return !string.IsNullOrEmpty(content) ? content.Trim('{', '}') : string.Empty;
        }

        #endregion
    }
}