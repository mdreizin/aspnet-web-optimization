﻿using System.Web;

namespace Web.Optimization.Core
{
    internal sealed class EmptyHttpContext : HttpContextBase
    {
        #region Fields

        private readonly HttpRequestBase httpRequest;

        #endregion

        #region Constructors

        public EmptyHttpContext(string virtualPath)
        {
            httpRequest = new EmptyHttpRequest(virtualPath);
        }

        #endregion

        #region Overrides of HttpContextBase

        public override HttpRequestBase Request
        {
            get { return httpRequest; }
        }

        #endregion
    }
}