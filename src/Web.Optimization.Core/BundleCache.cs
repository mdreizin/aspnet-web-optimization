﻿namespace Web.Optimization.Core
{
    public static class BundleCache
    {
        #region Fields

        private static readonly IBundleCache defaultCache = new DefaultBundleCache();

        private static IBundleCache currentCache;

        #endregion

        #region Properties

        public static IBundleCache Current
        {
            get { return currentCache ?? defaultCache; }
            set { currentCache = value; }
        }

        #endregion
    }
}