﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Optimization;

namespace Web.Optimization.Core
{
    internal sealed class DefaultBundleActivator : IBundleActivator
    {
        #region IBundleActivator Members

        public DefaultBundle CreateBundle(BundleDefinition bundleDefinition)
        {
            if (bundleDefinition == null)
            {
                throw new ArgumentNullException("bundleDefinition");
            }

            DefaultBundle bundle;

            try
            {
                bundle = CreateInstance<DefaultBundle>(bundleDefinition.Type, bundleDefinition.Path);
            }
            catch (Exception)
            {
                try
                {
                    bundle = CreateInstance<DefaultBundle>(bundleDefinition.Type);
                }
                catch (Exception)
                {
                    bundle = null;
                }
            }

            if (bundle != null)
            {
                UpdateBundle(bundle, bundleDefinition);
            }

            return bundle;
        }

        #endregion

        #region Methods

        private static void UpdateBundle(Bundle bundle, BundleDefinition bundleDefinition)
        {
            UpdateBundleProperties(bundle, bundleDefinition);
            AddBundleTransforms(bundle, bundleDefinition.Transforms);
            AddBundleIncludes(bundle, bundleDefinition.Includes);
        }

        private static void UpdateBundleProperties(Bundle bundle, BundleDefinition bundleDefinition)
        {
            if (!string.IsNullOrEmpty(bundleDefinition.CdnPath))
            {
                bundle.CdnPath = bundleDefinition.CdnPath;
            }

            if (!string.IsNullOrEmpty(bundleDefinition.CdnFallbackExpression))
            {
                bundle.CdnFallbackExpression = bundleDefinition.CdnFallbackExpression;
            }

            if (!string.IsNullOrEmpty(bundleDefinition.ConcatenationToken))
            {
                bundle.ConcatenationToken = bundleDefinition.ConcatenationToken;
            }

            if (!string.IsNullOrEmpty(bundleDefinition.Builder))
            {
                bundle.Builder = CreateInstance<IBundleBuilder>(bundleDefinition.Builder);
            }

            if (!string.IsNullOrEmpty(bundleDefinition.Orderer))
            {
                bundle.Orderer = CreateInstance<IBundleOrderer>(bundleDefinition.Orderer);
            }
        }

        private static void AddBundleTransforms(Bundle bundle, IEnumerable<string> types)
        {
            var transforms = CreateInstances<IBundleTransform>(types).Where(x => x != null);

            foreach (var transform in transforms)
            {
                bundle.Transforms.Add(transform);
            }
        }

        private static IEnumerable<IItemTransform> GetItemTransforms(IEnumerable<string> types)
        {
            return CreateInstances<IItemTransform>(types).Where(x => x != null);
        }

        private static void AddBundleIncludes(Bundle bundle, IEnumerable<BundleAssetDefinition> assets)
        {
            var includes = assets.Where(x => x != null);

            foreach (var include in includes)
            {
                var transforms = GetItemTransforms(include.Transforms);

                bundle.Include(include.Path, transforms.ToArray());
            }
        }

        private static IEnumerable<T> CreateInstances<T>(IEnumerable<string> types) where T : class
        {
            if (types != null)
            {
                foreach (var type in types)
                {
                    yield return CreateInstance<T>(type);
                }
            }
        }

        private static T CreateInstance<T>(string typeName, params object[] arguments) where T : class
        {
            return CreateInstance<T>(Type.GetType(typeName), arguments);
        }

        private static T CreateInstance<T>(Type type, params object[] arguments) where T : class
        {
            var value = default(T);

            if (type != null && typeof(T).IsAssignableFrom(type))
            {
                value = Activator.CreateInstance(type, arguments) as T;
            }

            return value;
        }

        #endregion
    }
}