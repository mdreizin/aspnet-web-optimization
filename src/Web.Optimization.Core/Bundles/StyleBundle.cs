﻿using System.Linq;
using System.Web.Optimization;
using CssRewriteUrlTransform = Web.Optimization.Core.Transforms.CssRewriteUrlTransform;

namespace Web.Optimization.Core.Bundles
{
    public class StyleBundle : DefaultBundle
    {
        #region Fields

        private static readonly IBundleTransform cssMinify = new CssMinify();

        private static readonly IItemTransform cssRewriteUrl = new CssRewriteUrlTransform();

        #endregion

        #region Constructors

        public StyleBundle(string virtualPath)
            : base(virtualPath, new[] { cssMinify })
        {
        }

        #endregion

        #region Overrides of DefaultBundle

        protected override void OnInclude(BundleItem item)
        {
            base.OnInclude(item);

            if (!item.Transforms.OfType<CssRewriteUrlTransform>().Any())
            {
                item.Transforms.Insert(0, cssRewriteUrl);
            }
        }

        #endregion
    }
}