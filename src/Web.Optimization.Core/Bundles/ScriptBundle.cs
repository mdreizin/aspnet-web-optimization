﻿using System;
using System.Web.Optimization;

namespace Web.Optimization.Core.Bundles
{
    public class ScriptBundle : DefaultBundle
    {
        #region Fields

        private static readonly IBundleTransform jsMinify = new JsMinify();

        #endregion

        #region Constructors

        public ScriptBundle(string virtualPath)
            : base(virtualPath, new[] { jsMinify })
        {
            ConcatenationToken = ";" + Environment.NewLine;
        }

        #endregion
    }
}