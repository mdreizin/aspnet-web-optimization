﻿using System.Linq;
using Web.Optimization.Core.Transforms;

namespace Web.Optimization.Core.Bundles
{
    public class CultureBundle : ScriptBundle
    {
        #region Fields

        private static readonly IBundleFilter jsonWrapper = new JsonClosureWrapper("JSC");

        private static readonly IBundleFilter jsonCombine = new JsonCombine();

        #endregion

        #region Constructors

        public CultureBundle(string virtualPath)
            : base(virtualPath)
        {
            ConcatenationToken = ",";

            Transforms.Insert(0, jsonCombine);
            Transforms.Insert(1, jsonWrapper);
        }

        #endregion

        #region Overrides of DefaultBundle

        protected override void OnInclude(BundleItem item)
        {
            base.OnInclude(item);

            if (!item.Transforms.OfType<JsonCombine>().Any())
            {
                item.Transforms.Insert(0, jsonCombine);
            }
        }

        #endregion
    }
}