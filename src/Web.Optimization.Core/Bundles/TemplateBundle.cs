﻿using Web.Optimization.Core.Transforms;

namespace Web.Optimization.Core.Bundles
{
    public class TemplateBundle : ScriptBundle
    {
        #region Fields

        private static readonly IBundleFilter htmlMinify = new HtmlMinify();

        private static readonly IBundleFilter jsonWrapper = new JsonClosureWrapper("JST");

        private static readonly IBundleFilter templateCompiler = new TemplateCompiler();

        #endregion

        #region Constructors

        public TemplateBundle(string virtualPath) : base(virtualPath)
        {
            Transforms.Insert(0, htmlMinify);
            Transforms.Insert(1, templateCompiler);
            Transforms.Insert(2, jsonWrapper);
        }

        #endregion
    }
}