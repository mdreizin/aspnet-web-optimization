﻿using System;
using System.Net;
using System.Web;
using System.Web.Optimization;
using System.Web.Routing;

namespace Web.Optimization.Core
{
    internal sealed class BundleHttpHandler : IHttpHandler
    {
        #region Constructors

        public BundleHttpHandler(RequestContext requestContext)
        {
            if (requestContext == null)
            {
                throw new ArgumentNullException("requestContext");
            }

            RequestContext = requestContext;
        }

        #endregion

        #region Properties

        public bool IsReusable
        {
            get { return false; }
        }

        private RequestContext RequestContext { get; set; }

        #endregion

        #region Methods

        public void ProcessRequest(HttpContext context)
        {
            var httpContext = new HttpContextWrapper(context);
            var bundleVirtualPath = httpContext.Request.AppRelativeCurrentExecutionFilePath + context.Request.PathInfo;
            var bundleContext = BundleContextFactory.CreateBundleContext(bundleVirtualPath, httpContext);

            ProcessRequest(bundleContext, RequestContext);
        }

        private static void ProcessRequest(BundleContext bundleContext, RequestContext requestContext)
        {
            var bundle = bundleContext.BundleCollection.GetBundleFor(requestContext.RouteData);

            if (bundle != null)
            {
                if (!string.IsNullOrEmpty(bundleContext.HttpContext.Request.Headers["If-Modified-Since"]))
                {
                    bundleContext.HttpContext.Response.StatusCode = (int)HttpStatusCode.NotModified;
                }
                else
                {
                    var bundleResponse = bundle.GenerateBundleResponse(bundleContext);

                    if (bundleResponse != null)
                    {
                        SetHeaders(bundleResponse, bundleContext);

                        bundleContext.HttpContext.Response.Write(bundleResponse.Content);
                    }
                }
            }
        }

        private static void SetHeaders(BundleResponse bundleResponse, BundleContext bundleContext)
        {
            if (!string.IsNullOrEmpty(bundleResponse.ContentType))
            {
                bundleContext.HttpContext.Response.ContentType = bundleResponse.ContentType;
            }

            if (!bundleContext.EnableInstrumentation)
            {
                var cache = bundleContext.HttpContext.Response.Cache;

                cache.SetCacheability(bundleResponse.Cacheability);
                cache.SetOmitVaryStar(true);
                cache.SetExpires(DateTime.Now.AddYears(1));
                cache.SetValidUntilExpires(true);
                cache.SetLastModified(DateTime.Now);
                cache.VaryByHeaders["User-Agent"] = true;
            }
        }

        #endregion
    }
}