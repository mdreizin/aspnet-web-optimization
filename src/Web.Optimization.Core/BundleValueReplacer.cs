﻿namespace Web.Optimization.Core
{
    public static class BundleValueReplacer
    {
        #region Fields

        private static readonly IBundleValueReplacer defaultReplacer = new DefaultBundleValueReplacer();

        private static IBundleValueReplacer currentReplacer;

        #endregion

        #region Properties

        public static IBundleValueReplacer Current
        {
            get { return currentReplacer ?? defaultReplacer; }
            set { currentReplacer = value; }
        }

        #endregion
    }
}