﻿using System;
using System.Collections.Generic;

namespace Web.Optimization.Core
{
    [Serializable]
    public class BundleManifestDefinition
    {
        #region Fields

        private BundleDefinitionDictionary bundles;

        private IList<BundleManifestDefinition> manifests;

        #endregion

        #region Properties

        public string Path { get; set; }

        public IList<BundleManifestDefinition> Manifests
        {
            get { return manifests ?? (manifests = new List<BundleManifestDefinition>()); }
            set { manifests = value; }
        }

        public BundleDefinitionDictionary Bundles
        {
            get { return bundles ?? (bundles = new BundleDefinitionDictionary()); }
            set { bundles = new BundleDefinitionDictionary(value); }
        }

        #endregion
    }
}