﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Web.Optimization.Core
{
    [Serializable]
    public class BundleDefinition
    {
        #region Fields

        private static readonly IEqualityComparer<BundleDefinition> pathComparer = new PathEqualityComparer();

        private IList<BundleAssetDefinition> includes;

        private IList<string> transforms;

        #endregion

        #region Properties

        public string Path { get; set; }

        public string CdnPath { get; set; }

        public string CdnFallbackExpression { get; set; }

        public string ConcatenationToken { get; set; }

        public string Alias { get; set; }

        public string Type { get; set; }

        public string Orderer { get; set; }

        public string Builder { get; set; }

        public IList<string> Transforms
        {
            get { return transforms ?? (transforms = new List<string>()); }
            set { transforms = value; }
        }

        public IList<BundleAssetDefinition> Includes
        {
            get { return includes ?? (includes = new List<BundleAssetDefinition>()); }
            set { includes = value; }
        }

        [JsonIgnore]
        public BundleManifestDefinition Manifest { get; set; }

        internal static IEqualityComparer<BundleDefinition> PathComparer
        {
            get { return pathComparer; }
        }

        #endregion

        #region Nested type: PathEqualityComparer

        private sealed class PathEqualityComparer : IEqualityComparer<BundleDefinition>
        {
            #region IEqualityComparer<BundleDefinition> Members

            public bool Equals(BundleDefinition x, BundleDefinition y)
            {
                if (ReferenceEquals(x, y))
                {
                    return true;
                }

                if (ReferenceEquals(x, null))
                {
                    return false;
                }

                if (ReferenceEquals(y, null))
                {
                    return false;
                }

                if (x.GetType() != y.GetType())
                {
                    return false;
                }

                return string.Equals(x.Path, y.Path, StringComparison.OrdinalIgnoreCase);
            }

            public int GetHashCode(BundleDefinition obj)
            {
                return obj.Path != null ? obj.Path.GetHashCode() : 0;
            }

            #endregion
        }

        #endregion
    }
}