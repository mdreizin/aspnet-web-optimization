﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Web.Optimization;
using System.Web.Routing;

namespace Web.Optimization.Core
{
    public class BundleManifestDefinitionCollection : IEnumerable<BundleManifestDefinition>
    {
        #region Fields

        private const string ManifestExtension = ".manifest";

        private const string ManifestWildcard = "*" + ManifestExtension;

        private readonly IDictionary<string, BundleManifestDefinition> registeredManifests = CreateCaseInsensitiveDictionary<BundleManifestDefinition>();

        private readonly IDictionary<string, IEnumerable<string>> relatedManifests = CreateCaseInsensitiveDictionary<IEnumerable<string>>();

        private readonly IDictionary<string, BundleDefinition> registeredBundles = CreateCaseInsensitiveDictionary<BundleDefinition>();

        private readonly IDictionary<string, IEnumerable<string>> registeredBundleManifests = CreateCaseInsensitiveDictionary<IEnumerable<string>>();

        private readonly BundleCollection bundleCollection;

        private readonly RouteCollection routeCollection;

        #endregion

        #region Constructors

        public BundleManifestDefinitionCollection(BundleCollection bundleCollection, RouteCollection routeCollection)
        {
            if (bundleCollection == null)
            {
                throw new ArgumentNullException("bundleCollection");
            }

            if (routeCollection == null)
            {
                throw new ArgumentNullException("routeCollection");
            }

            this.bundleCollection = bundleCollection;
            this.routeCollection = routeCollection;
        }

        #endregion

        #region Properties

        public bool Sync { get; set; }

        internal BundleCollection BundleCollection
        {
            get { return bundleCollection; }
        }

        internal RouteCollection RouteCollection
        {
            get { return routeCollection; }
        }

        #endregion

        #region Indexers

        public BundleManifestDefinition this[string path]
        {
            get
            {
                BundleManifestDefinition manifest;

                registeredManifests.TryGetValue(path, out manifest);

                return manifest;
            }
        }

        #endregion

        #region IEnumerable<BundleManifestDefinition> Members

        public IEnumerator<BundleManifestDefinition> GetEnumerator()
        {
            return registeredManifests.Values.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        #endregion

        #region Methods

        public void Add(BundleManifestDefinition manifest)
        {
            if (manifest == null)
            {
                throw new ArgumentNullException("manifest");
            }

            Register(manifest);
        }

        public void Remove(BundleManifestDefinition manifest)
        {
            if (manifest == null)
            {
                throw new ArgumentNullException("manifest");
            }

            Unregister(manifest);
        }

        public void Require(string path)
        {
            if (string.IsNullOrEmpty(path))
            {
                throw new ArgumentNullException("path");
            }

            var manifest = Get(path);

            if (manifest == null)
            {
                manifest = BundleManifest.Provider.LoadManifest(path);

                if (manifest != null)
                {
                    Add(manifest);
                }
            }
        }

        public void Unrequired(string path)
        {
            if (string.IsNullOrEmpty(path))
            {
                throw new ArgumentNullException("path");
            }

            var manifest = Get(path);

            if (manifest != null)
            {
                Remove(manifest);
            }
        }

        public bool Exists(string path)
        {
            if (string.IsNullOrEmpty(path))
            {
                throw new ArgumentNullException("path");
            }

            var manifest = Get(path);

            return manifest != null;
        }

        public BundleManifestDefinition Closest(string path)
        {
            var relatedMap = relatedManifests.FirstOrDefault(x => x.Value.Contains(path, StringComparer.OrdinalIgnoreCase));

            return !string.IsNullOrEmpty(relatedMap.Key) ? this[relatedMap.Key] : null;
        }

        public void Reload(string path)
        {
            if (string.IsNullOrEmpty(path))
            {
                throw new ArgumentNullException("path");
            }

            var manifest = Get(path);

            if (IsManifestPath(manifest.Path))
            {
                Unrequired(path);
                Require(path);
            }
            else
            {
                Remove(manifest);
                Add(manifest);
            }
        }

        public void ReloadAll()
        {
            var manifests = GetRegisteredManifests();

            foreach (var manifest in manifests)
            {
                Reload(manifest.Path);
            }
        }

        public void ResetAll()
        {
            ResetManifests();
            ResetBundles();
        }

        public void SyncAll()
        {
            if (!BundleTable.EnableOptimizations && Sync)
            {
                ReloadAll();

                Sync = false;
            }
        }

        public IEnumerable<BundleManifestDefinition> GetRegisteredManifests()
        {
            return new ReadOnlyCollectionBuilder<BundleManifestDefinition>(registeredManifests.Values).ToReadOnlyCollection();
        }

        public IEnumerable<BundleDefinition> GetRegisteredBundles()
        {
            return new ReadOnlyCollectionBuilder<BundleDefinition>(registeredBundles.Values).ToReadOnlyCollection();
        }

        public IEnumerable<string> GetBundleManifests(string path)
        {
            IEnumerable<string> manifests;

            registeredBundleManifests.TryGetValue(path, out manifests);

            return manifests ?? Enumerable.Empty<string>();
        }

        public BundleDefinition GetBundleFor(string path)
        {
            BundleDefinition bundle;

            registeredBundles.TryGetValue(path, out bundle);

            return bundle;
        }

        private static void Update(Bundle bundle, BundleDefinition bundleDefinition)
        {
            if (bundle != null)
            {
                var includes = bundleDefinition.Includes;

                foreach (var include in includes)
                {
                    bundle.Include(include.Path);
                }
            }
        }

        private static IDictionary<string, T> CreateCaseInsensitiveDictionary<T>()
        {
            return new Dictionary<string, T>(StringComparer.OrdinalIgnoreCase);
        }

        private static bool IsManifestPath(string path)
        {
            return !string.IsNullOrEmpty(path) && path.EndsWith(ManifestExtension, StringComparison.OrdinalIgnoreCase);
        }

        private static void CheckPrerequisites(BundleCollection bundleCollection, Bundle bundle)
        {
            if (bundle != null)
            {
                bundle.Builder = !(bundle.Builder is DefaultBundleBuilder) ? DefaultBundleBuilder.Instance : bundle.Builder;
            }

            if (!(BundleResolver.Current is DefaultBundleResolver))
            {
                bundleCollection.IgnoreList.Ignore(ManifestWildcard, OptimizationMode.Always);

                BundleResolver.Current = new DefaultBundleResolver(BundleResolver.Current);
            }
        }

        private BundleManifestDefinition Get(string path)
        {
            var manifest = this[path] ?? Closest(path);

            return manifest;
        }

        private void ResetManifests()
        {
            var manifests = GetRegisteredManifests();

            foreach (var manifest in manifests)
            {
                Unregister(manifest);
            }

            registeredManifests.Clear();
            relatedManifests.Clear();
        }

        private void ResetBundles()
        {
            var bundles = GetRegisteredBundles();

            foreach (var bundle in bundles)
            {
                Unregister(bundle);
            }

            registeredBundles.Clear();
            registeredBundleManifests.Clear();
        }

        private void Register(BundleManifestDefinition manifest)
        {
            if (manifest != null && !string.IsNullOrEmpty(manifest.Path))
            {
                manifest.Inspect(Register);

                registeredManifests[manifest.Path] = manifest;
                relatedManifests[manifest.Path] = manifest.GetRelatedManifests().Select(x => x.Path);
            }
        }

        private void Register(BundleDefinition bundleDefinition)
        {
            if (bundleDefinition != null && !string.IsNullOrEmpty(bundleDefinition.Path))
            {
                var exists = registeredBundles.ContainsKey(bundleDefinition.Path);
                var bundle = exists ? bundleCollection.GetBundleFor(bundleDefinition.Path) : BundleFactory.CreateBundle(bundleDefinition);

                if (bundle != null)
                {
                    var manifestDefinition = bundleDefinition.Manifest;

                    if (!exists)
                    {
                        Register(bundle, bundleDefinition);
                    }
                    else
                    {
                        Update(bundle, bundleDefinition);
                    }

                    if (manifestDefinition != null && !string.IsNullOrEmpty(manifestDefinition.Path))
                    {
                        IEnumerable<string> manifests;

                        var bundleManifests = new HashSet<string>(StringComparer.OrdinalIgnoreCase) { manifestDefinition.Path };

                        if (relatedManifests.TryGetValue(manifestDefinition.Path, out manifests))
                        {
                            foreach (var manifest in manifests)
                            {
                                bundleManifests.Add(manifest);
                            }
                        }

                        if (!exists)
                        {
                            manifests = manifestDefinition.GetRelatedManifests().Select(x => x.Path);

                            foreach (var manifest in manifests)
                            {
                                bundleManifests.Add(manifest);
                            }
                        }

                        registeredBundleManifests[bundle.Path] = bundleManifests.Where(IsManifestPath);
                    }

                    CheckPrerequisites(bundleCollection, bundle);
                }
            }
        }

        private void Register(Bundle bundle, BundleDefinition bundleDefinition)
        {
            if (bundle != null && bundleDefinition != null)
            {
                bundleCollection.Add(bundle);

                using (routeCollection.GetWriteLock())
                {
                    routeCollection.MapBundle(bundle.Path, bundle.Path);
                }

                registeredBundles[bundle.Path] = bundleDefinition;
            }
        }

        private void Unregister(BundleManifestDefinition manifest)
        {
            if (manifest != null && !string.IsNullOrEmpty(manifest.Path))
            {
                manifest.Inspect(Unregister);

                registeredManifests.Remove(manifest.Path);
                relatedManifests.Remove(manifest.Path);
            }
        }

        private void Unregister(BundleDefinition bundle)
        {
            if (bundle != null && !string.IsNullOrEmpty(bundle.Path))
            {
                Unregister(bundleCollection.GetBundleFor(bundle.Path));

                registeredBundles.Remove(bundle.Path);
                registeredBundleManifests.Remove(bundle.Path);
            }
        }

        private void Unregister(Bundle bundle)
        {
            if (bundle != null)
            {
                bundleCollection.Remove(bundle);

                using (routeCollection.GetWriteLock())
                {
                    var route = routeCollection[bundle.Path];

                    if (route != null)
                    {
                        routeCollection.Remove(route);
                    }
                }
            }
        }

        #endregion
    }
}