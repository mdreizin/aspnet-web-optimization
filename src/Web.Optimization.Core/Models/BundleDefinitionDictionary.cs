﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Web.Optimization.Core
{
    [Serializable]
    public class BundleDefinitionDictionary : Dictionary<string, IList<BundleDefinition>>
    {
        #region Constructors

        public BundleDefinitionDictionary()
        {
        }

        public BundleDefinitionDictionary(IDictionary<string, IList<BundleDefinition>> dictionary = null)
            : base(dictionary ?? new Dictionary<string, IList<BundleDefinition>>(), StringComparer.OrdinalIgnoreCase)
        {
        }

        protected BundleDefinitionDictionary(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }

        #endregion

        #region Indexers

        public new IList<BundleDefinition> this[string key]
        {
            get
            {
                IList<BundleDefinition> list;

                if (!TryGetValue(key, out list))
                {
                    list = new List<BundleDefinition>();

                    base[key] = list;
                }

                return list;
            }

            set
            {
                base[key] = value;
            }
        }

        #endregion
    }
}