﻿using System;
using System.Collections.Generic;

namespace Web.Optimization.Core
{
    [Serializable]
    public class BundleAssetDefinition
    {
        #region Fields

        private IList<string> transforms;

        #endregion

        #region Properties

        public string Path { get; set; }

        public IList<string> Transforms
        {
            get { return transforms ?? (transforms = new List<string>()); }
            set { transforms = value; }
        }

        #endregion
    }
}