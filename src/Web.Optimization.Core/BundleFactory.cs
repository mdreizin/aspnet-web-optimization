﻿using System;
using System.Web.Optimization;

namespace Web.Optimization.Core
{
    internal static class BundleFactory
    {
        #region Methods

        public static Bundle CreateBundle(BundleDefinition bundle)
        {
            if (bundle == null)
            {
                throw new ArgumentNullException("bundle");
            }

            if (string.IsNullOrEmpty(bundle.Type))
            {
                var type = BundleActivators.Activators.FindAssociatedType(bundle.Alias);

                if (type != null)
                {
                    bundle.Type = type.AssemblyQualifiedName;
                }
            }

            var activator = BundleActivators.Activators.GetActivator(bundle.Type);

            return activator != null ? activator.CreateBundle(bundle) : null;
        } 

        #endregion
    }
}