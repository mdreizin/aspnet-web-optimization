﻿using System;
using System.Collections.Generic;
using System.Web.Optimization;

namespace Web.Optimization.Core
{
    public class BundleItem
    {
        #region Fields

        private static readonly IEqualityComparer<BundleItem> virtualPathComparer = new VirtualPathEqualityComparer();

        private IList<IItemTransform> transforms;

        #endregion

        #region Properties

        public string VirtualPath { get; set; }

        public IList<IItemTransform> Transforms
        {
            get { return transforms ?? (transforms = new List<IItemTransform>()); }
        }

        internal static IEqualityComparer<BundleItem> VirtualPathComparer
        {
            get { return virtualPathComparer; }
        }

        #endregion

        #region Methods

        public virtual void AddFiles(IList<BundleFile> files, BundleContext bundleContext)
        {
            if (files == null)
            {
                throw new ArgumentNullException("files");
            }

            if (bundleContext == null)
            {
                throw new ArgumentNullException("bundleContext");
            }

            var virtualPath = BundleValueReplacer.Current.ReplaceValues(bundleContext, VirtualPath);

            if (BundleTable.VirtualPathProvider.FileExists(virtualPath))
            {
                var file = new BundleFile(virtualPath, BundleTable.VirtualPathProvider.GetFile(virtualPath), Transforms);

                files.Add(file);
            }
        }

        #endregion

        #region Nested type: VirtualPathEqualityComparer

        private sealed class VirtualPathEqualityComparer : IEqualityComparer<BundleItem>
        {
            #region IEqualityComparer<BundleItem> Members

            public bool Equals(BundleItem x, BundleItem y)
            {
                if (ReferenceEquals(x, y))
                {
                    return true;
                }

                if (ReferenceEquals(x, null))
                {
                    return false;
                }

                if (ReferenceEquals(y, null))
                {
                    return false;
                }

                if (x.GetType() != y.GetType())
                {
                    return false;
                }

                return string.Equals(x.VirtualPath, y.VirtualPath, StringComparison.OrdinalIgnoreCase);
            }

            public int GetHashCode(BundleItem obj)
            {
                return obj.VirtualPath != null ? obj.VirtualPath.GetHashCode() : 0;
            }

            #endregion
        }

        #endregion
    }
}