﻿using System.Web.Optimization;

namespace Web.Optimization.Core
{
    internal interface IBundleFilter : IBundleTransform, IItemTransform
    {
    }
}