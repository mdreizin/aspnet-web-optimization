﻿using System.Web.Optimization;

namespace Web.Optimization.Core
{
    public interface IBundleValueProviderFactory
    {
        #region Methods

        IBundleValueProvider GetValueProvider(BundleContext bundleContext);

        #endregion
    }
}