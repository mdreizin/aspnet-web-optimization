﻿namespace Web.Optimization.Core
{
    public interface IBundleManifestProvider
    {
        #region Methods

        BundleManifestDefinition LoadManifest(string path);

        #endregion
    }
}