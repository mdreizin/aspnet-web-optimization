﻿using Web.Optimization.Core.Bundles;

namespace Web.Optimization.Core
{
    public static class BundleActivators
    {
        #region Fields

        private static BundleActivatorDictionary activators;

        #endregion

        #region Properties

        public static BundleActivatorDictionary Activators
        {
            get { return activators ?? (activators = CreateDefaultActivators()); }
        }

        #endregion

        #region Methods

        private static BundleActivatorDictionary CreateDefaultActivators()
        {
            var defaultActivators = new BundleActivatorDictionary();

            defaultActivators.AddAssociation<ScriptBundle>("scripts");
            defaultActivators.AddAssociation<StyleBundle>("styles");
            defaultActivators.AddAssociation<CultureBundle>("cultures");
            defaultActivators.AddAssociation<TemplateBundle>("templates");

            return defaultActivators;
        }

        #endregion
    }
}